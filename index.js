#!/usr/bin/env node

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

const commander = require('commander');

commander
  .version('1.0.0')
  .option('--host [host]', 'connect to specified host', 'http://localhost:8080')
  .parse(process.argv);

const socket = require('socket.io-client')(commander.host);

readline.on('line', line=>{
  const endpoint = line.substr(0, line.indexOf(' '));
  const message = JSON.parse(line.substr(line.indexOf(' ')+1));
  socket.emit(endpoint, message);
});

socket.on('connect', ()=>{
  console.log('CONNECT {}');
});

socket.on('disconnect', ()=>{
  console.log('DISCONNECT {}');
});

socket.on('t', data=>{
  console.log('TERRAIN '+JSON.stringify(data));
});

socket.on('chat', data=>{
  console.log('CHAT '+JSON.stringify(data));
});

socket.on('loginsuccess', data=>{
  console.log('LOGINSUCCESS '+JSON.stringify(data));
});

socket.on('loginerror', data=>{
  console.log('LOGINERROR '+JSON.stringify(data));
});

