# GRINTER

GRINTER - AAGRINDER Interface (https://gitlab.com/MRAAGH/aagrinder)

Minimalistic console client for interfacing with the AAGRINDER server.

# Installation

You need to install [node.js](https://nodejs.org/en/).

Then run the following commands:
```
$ git clone https://gitlab.com/MRAAGH/grinter.git
$ cd grinter
$ npm install
```

Run the following command (as root) to install GRINTER globally:
```
# npm install -g
```

# Example usage

Using the file `test.txt` containing:
```
login {"username":"me","password":"mypass"}
chat {"message":"hello"}
```

You can run:
```
$ cat test.txt | GRINTER
```

This logs into the server and sends a message in chat.
Expect quite a lot of output, as the server will send you terrain data.
But as the last line, you'll see a received chat message:
```
CHAT {"message":"me: hello"}
```

In case you tried with a nonexistent user and the server is runnig in secure mode, you will get this instead:
```
CONNECT {}
LOGINERROR {"message":"User does not exist. Try /register or /r"}
```

And if the server is inaccessible, you'll just get nothing.

# Basic documentation

`GRINTER` takes a `--host` argument.
With it, you can specify some other server than `http://localhost:8080`.

## Inputs

Each line on stdin represents a message to be sent over socket.io

Each line should consist of an endpoint name and a JSON-encoded object to be sent to that endpoint; separated by exactly one space.

The endpoints the server responds to at the time of writing are:
- `login`
- `chat`
- `a`

## Outputs

All received socket traffic is sent to stdout in the following format:

`ENDPOINT {JSONcontent}`

Possible received endpoints:
- `CONNECT {}`
- `DISCONNECT {}`
- `TERRAIN {JSONcontent}`
- `CHAT {JSONcontent}`
- `LOGINSUCCESS {JSONcontent}`
- `LOGINERROR {JSONcontent}`

